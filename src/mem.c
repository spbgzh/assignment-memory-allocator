#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

//在mem_internals.h中实现
extern inline block_size
size_from_capacity(block_capacity
                       cap);
extern inline block_capacity
capacity_from_size(block_size
                       sz);

//判断块是否够大
static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

//计算页数
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

//页边界
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

//初始化块
static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
    *((struct block_header *)addr) = (struct block_header){
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true};
}

//内存区域实际大小
static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

//判断内存区域是否有效，已在mem.h中实现
extern inline bool

region_is_invalid(const struct region *r);

//页映射
static void *map_pages(void const *addr, size_t length, int additional_flags)
{
    return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
/* 分配一个内存区域并用一个块对其进行初始化 */
static struct region alloc_region(void const *addr, size_t query)
{
    size_t actual_size = region_actual_size(query);
    // MAP_FIXED:interpret addr exactly
    void *p_map_addr = map_pages(addr, actual_size, MAP_FIXED);

    struct region init_region;
    if (p_map_addr != MAP_FAILED)
    {
        init_region = (struct region){p_map_addr, query, true};
    }
    else
    {
        // MAP_FILE: map from file (default)
        p_map_addr = map_pages(addr, actual_size, MAP_FILE);
        init_region = (struct region){p_map_addr, query, false};
    }
    block_size init_size = {query};
    block_init(p_map_addr, init_size, NULL);
    return init_region;
}

//获得下一个块的指针,在下文实现
static void *block_after(struct block_header const *block);

//堆初始化
void *heap_init(size_t initial)
{
    //分配内存区域给堆区
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region))
        return NULL;
    return region.addr;
}

//最小的块容量
#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
/* 拆分块（如果找到的空闲块太大） */
//判断块是否可以拆分
static bool block_splittable(struct block_header *restrict block, size_t query)
{
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//进行拆分返回是否拆分成功
static bool split_if_too_big(struct block_header *block, size_t query)
{
    if (block_splittable(block, query))
    {
        struct block_header *p_next_block =
            (struct block_header *)(((size_t)block + offsetof(struct block_header, contents)) + query);
        block_init(p_next_block, (block_size){block->capacity.bytes - query}, block->next);
        block->capacity.bytes = query;
        block->next = p_next_block;
        return true;
    }
    return false;
}

/*  --- Слияние соседних свободных блоков --- */
/* 合并相邻的空闲块 */
//获得下一个块的指针
static void *block_after(struct block_header const *block)
{
    return (void *)(block->contents + block->capacity.bytes);
}

//判断块是否连续
static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd)
{
    return (void *)snd == block_after(fst);
}

//判断是否可以合并
static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

//尝试进行合并
static bool try_merge_with_next(struct block_header *block)
{
    if (mergeable(block, block->next))
    {
        block->capacity.bytes = block->capacity.bytes + block->next->capacity.bytes;
        block->next = block->next->next;
        return true;
    }
    return false;
}

/*  --- ... ecли размера кучи хватает --- */
/* 返回状态码堆的大小是否足够 */
struct block_search_result
{
    enum
    {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED,
        SPLIT_ERROR,
        GROW_HEAP_AND_BLOCK_ALLOCATED
    } type;
    struct block_header *block;
};

//快慢指针判断是否为环
bool detectIsCircleByFastSlow(struct block_header *block)
{
    struct block_header *fast = block;
    struct block_header *slow = block;
    while (fast->next != NULL && fast->next->next != NULL)
    {
        fast = fast->next->next;
        slow = slow->next;
        if (fast == slow)
        {
            return false;
        }
    }
    return true;
}

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz)
{
    if (detectIsCircleByFastSlow(block))
        return (struct block_search_result){BSR_CORRUPTED, block};
        
    while (block->next)
    {
        if (block->is_free && block_is_big_enough(sz, block))
            return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
        if (!try_merge_with_next(block))
            block = block->next;
    }

    if (block->is_free && block_is_big_enough(sz, block))
        return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
    return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
/* 尝试从块`block`开始在堆上分配内存而不尝试扩展堆
  堆扩展后可以立即重用。
  */

//扩展堆
static struct block_header *grow_heap(struct block_header *restrict last, size_t query)
{
    struct region extended_area = alloc_region(block_after(last), query);
    last->next = extended_area.addr;
    return extended_area.addr;
}

//尝试在已存在的块上分配内存
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block)
{
    struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK)
    {
        if (split_if_too_big(search_result.block, query))
            return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
        else
            return (struct block_search_result){SPLIT_ERROR, block};
    }
    else if (search_result.type == BSR_REACHED_END_NOT_FOUND)
    {
        struct block_header *grow_block = grow_heap(search_result.block, query);
        return (struct block_search_result){GROW_HEAP_AND_BLOCK_ALLOCATED, grow_block};
    }
    else
    {
        return search_result;
    }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
/* 实现主要的 malloc 逻辑并返回分配块的头部 */
static struct block_header *memalloc(size_t query, struct block_header *heap_start)
{
    query = (query < BLOCK_MIN_CAPACITY) ? BLOCK_MIN_CAPACITY : query;
    struct block_search_result result_of_allocate_mem = try_memalloc_existing(query, heap_start);
    if (result_of_allocate_mem.type == BSR_REACHED_END_NOT_FOUND)
    {
        grow_heap(result_of_allocate_mem.block, query);
        result_of_allocate_mem = try_memalloc_existing(query, heap_start);
    }
    result_of_allocate_mem.block->is_free = false;
    return result_of_allocate_mem.block;
}

//分配
void *_malloc(size_t query)
{
    struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
    if (addr)
        return addr->contents;
    else
        return NULL;
}

//得到块头
static struct block_header *block_get_header(void *contents)
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(
                                                               struct block_header, contents));
}

//释放内存
void _free(void *mem)
{
    if (!mem)
        return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while ( try_merge_with_next(header));
}
