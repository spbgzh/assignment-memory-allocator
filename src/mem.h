#ifndef _MEM_H_
#define _MEM_H_


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <sys/mman.h>

//堆区的起始虚拟内存
#define HEAP_START ((void*)0x04040000)

//分配内存
void *_malloc(size_t query);

//释放内存
void _free(void *mem);

//堆区的初始化
void *heap_init(size_t initial_size);

#define DEBUG_FIRST_BYTES 4

//debug结构体信息
void debug_struct_info(FILE *f, void const *address);

//debug堆
void debug_heap(FILE *f, void const *ptr);

#endif
