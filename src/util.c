#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

//_Noreturn是C11的保留字，保证不返回任何东西，优化编译
_Noreturn void err(const char *msg, ...) {
    va_list args;
    va_start(args, msg);
    vfprintf(stderr, msg, args);
    va_end(args);
    abort();
}

//返回x，y中更大的数字
extern inline size_t
size_max(size_t x, size_t y);
