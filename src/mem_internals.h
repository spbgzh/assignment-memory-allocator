#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>

#define REGION_MIN_SIZE (2 * 4096)

//内存区
struct region {
    void *addr;
    size_t size;
    bool extends;
};
static const struct region REGION_INVALID = {0};

//判断内存区是否有效
inline bool region_is_invalid(const struct region *r) { return r->addr == NULL; }


//块容量
typedef struct {
    size_t bytes;
} block_capacity;

//块实际大小
typedef struct {
    size_t bytes;
} block_size;

//块头
struct block_header {
    struct block_header *next;   //块链表的下一个地址
    block_capacity capacity;     //块容量
    bool is_free;      //是否被占用
    uint8_t contents[];   //内容
};

//通过块容量获取块大小
inline block_size size_from_capacity(block_capacity cap) {
    return (block_size) {cap.bytes + offsetof( struct block_header, contents )};
}

//通过块大小获取块容量
inline block_capacity capacity_from_size(block_size sz) {
    return (block_capacity) {sz.bytes - offsetof( struct block_header, contents )};
}

#endif
